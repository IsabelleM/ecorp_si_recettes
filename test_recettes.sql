use restaurants;


#Ajouter une recette
INSERT INTO recipe (recipe_name) VALUES ("crêpes");

INSERT INTO product_recipe (product_id,recipe_id,quantity) VALUES 
(6, (SELECT product_id FROM product WHERE product_name = "farine"), 100),
(6, (SELECT product_id FROM product WHERE product_name = "oeuf"), 3),
(6, (SELECT product_id FROM product WHERE product_name = "sucre"), 100),
(6, (SELECT product_id FROM product WHERE product_name = "lait"), 100),
(6, (SELECT product_id FROM product WHERE product_name = "dopamine"), 1);


INSERT INTO recipe (recipe_name) VALUES ("gâteau au chocolat");

INSERT INTO product_recipe (product_id,recipe_id,quantity) VALUES 
(7, (SELECT product_id FROM product WHERE product_name = "farine"), 50),
(7, (SELECT product_id FROM product WHERE product_name = "oeuf"), 3),
(7, (SELECT product_id FROM product WHERE product_name = "sucre"), 100),
(7, (SELECT product_id FROM product WHERE product_name = "beurre"), 25);




 