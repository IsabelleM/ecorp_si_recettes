import pandas as pd     #importer la librairie pandas
import mysql.connector  #import du connecteur mysql

#Lire le fichier csv et le transformer en dataframe
df_C = pd.read_csv("extract_C.csv")
df_C = pd.DataFrame(df_C)

#Examiner les caractéristiques du dataframe
df_C.info()

#Remplacer les é par e et œufs par oeuf
df_C["product"] = df_C["product"].str.replace("é", "e")
df_C["product"] = df_C["product"].str.replace("œufs", "oeuf")

#Mettre le nom des produits en minuscules
df_C["product"] = df_C["product"].str.lower()

#Trier les valeurs par produit
df_C.sort_values(by='product')

#Remplacer les valeurs négatives par O
df_C.loc[df_C["stock"] < 0, "stock"] = 0

#Grouper par produit en en faisant la somme
summed_df_C = df_C.groupby('product')['stock'].sum().reset_index()


#Créer une instance de connection
connection = mysql.connector.connect(
    host="localhost",
    user="david",
    password="coucou123",
    database="restaurants"
)

# Créer un curseur pour exécuter des commandes SQL
cursor = connection.cursor()


# Ajouter le restaurant C
cursor.execute("INSERT INTO restaurant (restaurant_name) VALUES ('restaurant_C');")
#Récuoérer l'ID du restaurant C
restaurant_C_id = cursor.lastrowid  

# Vérifier et ajouter les produits qui n'existent pas déjà pour le restaurant C
for index, row in summed_df_C.iterrows():
    product_name = row['product'].replace("'", "\'")

    # Vérifier si le produit existe déjà dans la base de données
    cursor.execute("SELECT product_id FROM product WHERE product_name = %s;", (product_name,))
    result = cursor.fetchone()

    if not result:
        # Si le produit n'existe pas, l'ajouter à la table des produits
        cursor.execute("INSERT INTO product (product_name) VALUES (%s);", (product_name,))
        #Récupérer l'ID du produit
        product_id = cursor.lastrowid
    else:
        product_id = result[0]
        # Ajouter une entrée dans la table des stocks
    cursor.execute("INSERT INTO stock (restaurant_id, product_id, quantity) VALUES (%s, %s, %s);",
                       (restaurant_C_id, product_id, row['stock']))

# Valider les changements
connection.commit()

# Fermer le curseur et la connexion
cursor.close()
connection.close()
