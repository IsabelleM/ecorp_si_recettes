DROP DATABASE IF EXISTS restaurants;

CREATE DATABASE IF NOT EXISTS restaurants;

USE restaurants;

CREATE TABLE product (
  product_id integer not null PRIMARY KEY AUTO_INCREMENT,
  product_name varchar(56)
);

CREATE TABLE restaurant (
  restaurant_id integer not null PRIMARY KEY AUTO_INCREMENT,
  restaurant_name varchar(56)
);

CREATE TABLE stock (
  stock_id integer not null PRIMARY KEY AUTO_INCREMENT,
  restaurant_id integer,
  product_id integer,
  quantity integer,
  date DATE,
  FOREIGN KEY (product_id) REFERENCES product (product_id),
  FOREIGN KEY (restaurant_id) REFERENCES restaurant (restaurant_id)
);


grant all privileges on *.* to 'david'@'localhost';