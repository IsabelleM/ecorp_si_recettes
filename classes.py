from pydantic import BaseModel
import datetime 

class Product(BaseModel):
    id: int | None = None
    name: str | None = None

class Restaurant(BaseModel):
    id: int | None = None
    name: str | None = None

class Stock(BaseModel):
    restaurant_id: int | None = None
    product_id: int | None = None
    quantity : int | None = None
    date : datetime.datetime | None = None

class Recipe(BaseModel):
    id: int | None = None
    name: str | None = None

class product_recipe(BaseModel):
    product_id: int | None = None
    recipe_id: int | None = None
    quantity: int | None = None