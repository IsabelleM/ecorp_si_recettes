from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from typing import List, Dict
from classes import Product, Restaurant, Stock, Recipe, product_recipe
import mysql.connector
import datetime

app = FastAPI()

mydb = mysql.connector.connect(
    host="localhost",
    user="david",
    password="coucou123",
    database="restaurants"
)

#Voir tous les produits:
@app.get("/product") 
def get_product():
    cursor = mydb.cursor(dictionary=True)
    cursor.execute(""" 
        SELECT * FROM product;
        """)
    return cursor.fetchall()

#Ajouter un nouveau produit:
@app.post("/product/{nouveau_produit}")
def create_product(nouveau_produit: str):
    cursor = mydb.cursor()
    cursor.execute(""" 
        insert into product(product_name) values (%s); 
        """, [nouveau_produit])
    mydb.commit()
    return cursor.lastrowid

#Voir toutes les recettes.
@app.get("/recette")
def get_recette():
    with mydb.cursor(dictionary=True) as cursor:
        cursor.execute("SELECT * FROM recipe")
        result = cursor.fetchall()
    return result

#Voir tous les restaurants:
@app.get("/restaurant")
def get_restaurant():
    cursor = mydb.cursor(dictionary=True)
    cursor.execute(""" 
        SELECT * FROM restaurant;
        """)
    return cursor.fetchall()


#Voir tous les stocks
@app.get("/stock")
def get_stock():
    cursor = mydb.cursor(dictionary=True)
    cursor.execute(""" 
        SELECT * FROM stock;
        """)
    return cursor.fetchall()

#En envoyant le nom d’un produit, connaître l’état des stocks dans chaque restaurant ainsi que le stock global (cumulé).
@app.get("/product_stock/{product_name}")
def get_product_stock(product_name:str):
    cursor = mydb.cursor(dictionary= True)
    cursor.execute("""SELECT product_id FROM product WHERE product_name = %s;""", (product_name,))
    product_id = cursor.fetchone()
    product_id = product_id["product_id"]
    print(product_id)

    cursor.execute("""
                SELECT restaurant.restaurant_name AS restaurant_name, stock.quantity
                FROM stock
                JOIN restaurant ON stock.restaurant_id = restaurant.restaurant_id
                WHERE stock.product_id = %s;
            """, (product_id,))
    restaurant_stocks = cursor.fetchall()

    # Récupérer le stock global (cumulé) du produit
    cursor.execute("SELECT SUM(quantity) as global_stock FROM stock WHERE product_id = %s;", (product_id,))
    global_stock = cursor.fetchone()["global_stock"]

    result = {
                "product_name": product_name,
                "restaurant_stocks": restaurant_stocks,
                "global_stock": global_stock
            }

    return result

# Nouvelle route pour augmenter ou diminuer le stock d'un produit dans un restaurant donné
@app.put("/update_stock/{product_name}/{restaurant_name}/{quantity_change}")
def update_stock(product_name: str,restaurant_name: str,quantity_change: int):
    cursor = mydb.cursor(dictionary= True)

    # Récupérer l'ID du produit en fonction du nom
    cursor.execute("SELECT product_id FROM product WHERE product_name = %s;", (product_name,))
    product_id = cursor.fetchone()
            
    product_id = product_id["product_id"]

    # Récupérer l'ID du restaurant en fonction du nom
    cursor.execute("SELECT restaurant_id FROM restaurant WHERE restaurant_name = %s;", (restaurant_name,))
    restaurant_id = cursor.fetchone()

    restaurant_id = restaurant_id["restaurant_id"]

    # Mettre à jour le stock dans la table stock
    cursor.execute("""
        UPDATE stock
        SET quantity = quantity + %s
        WHERE product_id = %s AND restaurant_id = %s;
        """, (quantity_change, product_id, restaurant_id))

    # Valider les changements
    mydb.commit()

    return {"message": f"Stock for product '{product_name}' in restaurant '{restaurant_name}' updated successfully."}

#Nouvelle route pour ajouter une recette
@app.post("/add_recipe/{recipe}")
def add_recipe(recipe: str):
    cursor = mydb.cursor()

    # Insérer une nouvelle recette
    cursor.execute("INSERT INTO recipe (recipe_name) VALUES (%s);", (recipe,))
    recipe_id = cursor.lastrowid

    # Parcourir la liste des ingrédients et quantités pour les ajouter à la base de données
    for ingredient, quantity in zip(recipe.ingredients, recipe.quantities):
    # Normaliser le nom de l'ingrédient
        normalized_ingredient = ingredient.replace("'", "\'")

        # Vérifier si le produit existe déjà dans la table product
        cursor.execute("SELECT product_id FROM product WHERE LOWER(product_name) = LOWER(%s);", (normalized_ingredient,))
        result = cursor.fetchone()

        if not result:
        # Si le produit n'existe pas, l'ajouter à la table des produits
            cursor.execute("INSERT INTO product (product_id, product_name) VALUES (NULL, %s);", (normalized_ingredient,))
            product_id = cursor.lastrowid
        else:
            product_id = result[0]

        # Insérer une entrée dans la table de liaison product_recipe
        cursor.execute("INSERT INTO product_recipe (product_id, recipe_id, quantity) VALUES (%s, %s, %s);", (product_id, recipe_id, quantity))

        # Valider les changements
        mydb.commit()

        return {"message": f"Recipe '{recipe.name}' added successfully."}
        

# Nouvelle route pour retrouver une recette par son nom
# @app.get("/find_recipe/{recipe_name}")
# def find_recipe(recipe_name: str):
#     cursor = mydb.cursor(dictionary=True)

#     # Rechercher la recette par son nom
#     cursor.execute("""SELECT
#                     recipe.recipe_id AS id,
#                     recipe.recipe_name AS name,
#                     GROUP_CONCAT(product.product_name) AS ingredients,
#                     GROUP_CONCAT(product_recipe.quantity) AS quantities
#                     FROM recipe
#                     LEFT JOIN product_recipe ON recipe.recipe_id = product_recipe.recipe_id
#                     LEFT JOIN product ON product_recipe.product_id = product.product_id
#                     WHERE LOWER(recipe.recipe_name) = LOWER(%s)
#                     GROUP BY recipe.recipe_id;
#             """, (recipe_name,))

#     results = cursor.fetchall()

#     #Rassembler les résultats pour créer l'objet Recipe
#     recipe_data = {
#         "id": results[0]["id"],
#         "name": results[0]["name"],
#         "ingredients": [result["ingredient"] for result in results if result["ingredient"]],
#         "quantities": [result["quantity"] for result in results if result["quantity"]]
#     }

#     recipe_result = Recipe(**recipe_data)

#     return recipe_result

