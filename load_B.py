import pandas as pd     #import da la librairie pandas
import mysql.connector  #import du connecteur mysql
from datetime import datetime   #import de la librairie datetime de pandas

#Créer une instance de connection
connection = mysql.connector.connect(
    host="localhost",
    user="david",
    password="coucou123",
    database="restaurants"
)

#Lire le fichier csv et le transformer en dataframe
df_B = pd.read_csv("extract_B.csv")
df_B = pd.DataFrame(df_B)

#Examiner les caractéristiques du dataframe
df_B.info()

#Regarder s'il y a des doublons
print(df_B.duplicated().to_string())

#Transformer les dates en format date (jour, mois, année)
df_B.info()
df_B["date"] = pd.to_datetime(df_B["date"], format='%d/%m/%Y', errors= "coerce")

#Renommer les colonnes
df_B.rename(columns={"stock":"product", "date":"date", "inventory":"stock"}, inplace= True)
df_B.head()

#Mettre le nom des produits en minuscules
df_B["product"] = df_B["product"].str.lower()

#Remplacer oeufs par oeuf
df_B["product"] = df_B["product"].str.replace("oeufs", "oeuf")
df_B["product"] = df_B["product"].str.replace("œufs", "oeuf")

#Trier les valeurs par date
df_B.sort_values(by='date', ascending = False)

# Grouper par produit et sélectionner la première valeur pour chaque groupe (la plus grande date)
grouped_df_B = df_B.groupby('product').first().reset_index()



# Créer un curseur pour exécuter des commandes SQL
cursor = connection.cursor()

# Ajouter le restaurant B
cursor.execute("INSERT INTO restaurant (restaurant_name) VALUES ('restaurant_B');")
#Récupérer l'id du restaurant B
restaurant_B_id = cursor.lastrowid

# Vérifier et ajouter les produits qui n'existent pas déjà pour le restaurant B
for index, row in grouped_df_B.iterrows():
    product_name = row['product'].replace("'", "\'")

    # Vérifier si le produit existe déjà dans la base de données
    cursor.execute("SELECT product_id FROM product WHERE product_name = %s;", (product_name,))
    result = cursor.fetchone()

    if not result:
        # Si le produit n'existe pas, l'ajouter à la table des produits
        cursor.execute("INSERT INTO product (product_name) VALUES (%s);", (product_name,))
        product_id = cursor.lastrowid
    else:
        product_id = result[0]
        # Ajouter une entrée dans la table des stocks
    cursor.execute("INSERT INTO stock (restaurant_id, product_id, quantity) VALUES (%s, %s, %s);", (restaurant_B_id, product_id, row['stock']))


# Valider les changements
connection.commit()

# Fermer le curseur et la connexion
cursor.close()
connection.close()