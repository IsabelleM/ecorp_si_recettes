USE restaurants;

CREATE TABLE recipe (
  recipe_id integer not null PRIMARY KEY AUTO_INCREMENT,
  recipe_name varchar(56)
);

CREATE TABLE product_recipe (
  product_id integer,
  recipe_id integer,
  quantity integer,
  FOREIGN KEY (product_id) REFERENCES product (product_id),
  FOREIGN KEY (recipe_id) REFERENCES recipe (recipe_id)
);

grant all privileges on *.* to 'david'@'localhost';