import pandas as pd
import mysql.connector 
from datetime import datetime

connection = mysql.connector.connect(
    host="localhost",
    user="david",
    password="coucou123",
    database="restaurants"
)

#Lire le fichier csv et le transormer en dataframe
df_R = pd.read_csv("recettes.csv")
df_R = pd.DataFrame(df_R)
print(df_R)

#Voir les caractéristiques du dataframe
df_R.info()

#Remplacer les é par e
df_R["ingredient"] = df_R["ingredient"].str.replace("é", "e")

#Tout mettre en minuscules
df_R["ingredient"] = df_R["ingredient"].str.lower()


#Remplacer oeufs par oeuf
df_R["ingredient"] = df_R["ingredient"].str.replace("oeufs", "oeuf")
df_R["ingredient"] = df_R["ingredient"].str.replace("œufs", "oeuf")

print(df_R)

# Créer un curseur pour exécuter des commandes SQL
cursor = connection.cursor()


# Boucle pour insérer les données dans la base de données
for index, row in df_R.iterrows():
    # Vérifier si la recette existe déjà dans la table "Recipes"
    cursor.execute("SELECT recipe_id FROM recipe WHERE recipe_name = %s", (row['name'],))
    result = cursor.fetchone()

    if result:
        # Si la recette existe, utilisez l'ID existant
        recipe_id = result[0]
    else:
        # Si la recette n'existe pas, insérez-la dans la table "Recipes"
        cursor.execute("INSERT IGNORE INTO recipe (recipe_name) VALUES (%s)", (row['name'],))
        # Valider les changements
        connection.commit()
        # Récupérer l'ID de la recette insérée
        recipe_id = cursor.lastrowid

    # Consume any unread result
    cursor.fetchall()

    #Normaliser le nom de l'ingrédient
    normalized_ingredient = row['ingredient'].replace("'", "\'")

    # Vérifier si le produit existe déjà dans la table product
    cursor.execute("SELECT product_id FROM product WHERE LOWER(product_name) = LOWER(%s);", (normalized_ingredient,))
    result = cursor.fetchone()

    if not result:
        # Si le produit n'existe pas, l'ajouter à la table des produits
        cursor.execute("INSERT INTO product (product_id, product_name) VALUES (NULL, %s);", (normalized_ingredient,))
        product_id = cursor.lastrowid
    else:
        product_id = result[0]

    # Insérer une entrée dans la table de liaison product_recipe
    cursor.execute("INSERT INTO product_recipe (product_id, recipe_id, quantity) VALUES (%s, %s, %s);", (product_id, recipe_id, row['spoon']))

    # Consume any unread result
    cursor.fetchall()

# Valider les changements
connection.commit()

# Fermer le curseur et la connexion
cursor.close()
connection.close()