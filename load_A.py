import pandas as pd     #import de la librairie pandas
import mysql.connector  #import du connecteur mysql

#Créer une instance de connection
connection = mysql.connector.connect(
    host="localhost",       #l'url de la base de données
    user="david",           #le nom d'utilisateur mysql
    password="coucou123",   #le mot de passe de l'utilisateur mysql
    database="restaurants"  #le nom de la base de données
)

#Lire le fichier csv et le transformer en dataframe
df_A = pd.read_csv("extract_A.csv")
df_A = pd.DataFrame(df_A)

#Voir les caractéristiques du dataframe
df_A.info()

#Remplacer les é par e
df_A["product"] = df_A["product"].str.replace("é", "e")

#Tout mettre en minuscules
df_A["product"] = df_A["product"].str.lower()
df_A

#Remplacer oeufs par oeuf et œufs par oeuf
df_A["product"] = df_A["product"].str.replace("oeufs", "oeuf")
df_A["product"] = df_A["product"].str.replace("œufs", "oeuf")

#Trier les valeurs par produit
df_A.sort_values(by='product', inplace=True)

#Faire la somme des produits
summed_df_A = df_A.groupby('product')['stock'].sum().reset_index()
print(summed_df_A)


#Créer un curseur pour exécuter des commandes SQL
cursor = connection.cursor()

#Ajouter le restaurant A
cursor.execute("INSERT INTO restaurant VALUES (NULL, 'restaurant_A');")
#Récupérer l'ID du restaurant
restaurant_id = cursor.lastrowid
print(restaurant_id)
connection.commit()

#Pour chaque colonne dans le dataframe:
for index, row in summed_df_A.iterrows():
    print(row)
    product_name = row['product'].replace("'", "\'")
    print(product_name)
    #Insérer les noms et id du produit dans la table produit
    cursor.execute("INSERT INTO product (product_id, product_name) VALUES (NULL, %s);", [product_name])
    #Récupère le dernier id du produit
    product_id = cursor.lastrowid
    connection.commit()
    #Insérer la quantité 
    cursor.execute("INSERT INTO stock (restaurant_id, product_id, quantity) VALUES (%s, %s, %s)", (restaurant_id, product_id, row['stock']))

# Valider les changements
connection.commit()

# Fermer le curseur et la connexion
cursor.close()
connection.close()


